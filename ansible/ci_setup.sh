#!/bin/sh
set -eu

basedir=$(dirname "$0")

if [ -z ${ANSIBLE_CONFIG+x} ]; then
  echo Missing ANSIBLE_CONFIG environment variable
  exit 1;
fi

mkdir -m 700 "$basedir/secrets"
echo "$ANSIBLE_DEPLOY_KEY" | base64 -d > "$basedir/secrets/id_ed25519"
chmod 400 "$basedir/secrets/id_ed25519"
