#!/bin/sh

cd /opt/app || exit
pip3 install -r requirements-test.txt

exec "$@"
