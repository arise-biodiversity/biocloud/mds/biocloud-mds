#!/bin/sh
docker pull "${CI_REGISTRY_IMAGE}:build-${CI_PIPELINE_ID}"
docker tag "${CI_REGISTRY_IMAGE}:build-${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
if [ "$CI_COMMIT_BRANCH" = "main" ]; then
    docker tag "${CI_REGISTRY_IMAGE}:build-${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}:latest";
fi
if [ "$CI_COMMIT_BRANCH" != "main" ]; then
    docker tag "${CI_REGISTRY_IMAGE}:build-${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}";
fi
if [ -n "${CI_COMMIT_TAG+x}" ]; then
    docker tag "${CI_REGISTRY_IMAGE}:build-${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}";
fi
docker push --all-tags "${CI_REGISTRY_IMAGE}"
