# Biocloud MDS

## Description

This is part of the biocloud setup.

## Configure MDS API

### Token
Using the MDS API requires a token. You need to request that using your credentials:

```bash
$ curl -X POST https://arisemdsvm.science.uva.nl/api-token-auth/ -d "username=<username>&password=<password>"

{"token": "some_value"}
```


### API

The MDS API has several resources. To get an overview:

URL: https://arisemdsvm.science.uva.nl/api/

```bash
$ curl https://arisemdsvm.science.uva.nl/api/ | jq
```

The following resources are used by this application:

#### Projects

URL: http://arisemdsvm.science.uva.nl/api/Project/

```bash
$ curl -H "Content-Type: application/json" \
       -H "Authorization: Token <my_token>" \
     https://arisemdsvm.science.uva.nl/api/Project/ | jq
```

#### Devices

URL: https://arisemdsvm.science.uva.nl/api/Device/

```bash
$ curl -H "Content-Type: application/json" \
       -H "Authorization: Token <my_token>" \
     https://arisemdsvm.science.uva.nl/api/Device/ | jq
```


#### Deployments

URL: https://arisemdsvm.science.uva.nl/api/Deployment/

```bash
$ curl -H "Content-Type: application/json" \
       -H "Authorization: Token <my_token>" \
     https://arisemdsvm.science.uva.nl/api/Deployment/ | jq
```

#### Files

URL: https://arisemdsvm.science.uva.nl/api/File/

```bash
$ curl -H "Content-Type: application/json" \
     -H "Authorization: Token <my_token>" \
     https://arisemdsvm.science.uva.nl/api/File/ | jq
```
**NOTE**: without a filter, this request will try to retrieve metadata of *all* files. Because this is a tremendous amount, you are advised to use a filter. For instance:

```bash
$ curl -H "Content-Type: application/json" \
     -H "Authorization: Token <my_token>" \
     'https://arisemdsvm.science.uva.nl/api/File/?start_date=2023-05-11' | jq
```

Or even multiple filters:
```bash
$ curl -H "Content-Type: application/json" \
     -H "Authorization: Token <my_token>" \
     'https://arisemdsvm.science.uva.nl/api/File/?start_date=2023-05-11&deployment__in=25' | jq

$ curl -H "Content-Type: application/json" \
     -H "Authorization: Token <my_token>" \
     'https://arisemdsvm.science.uva.nl/api/File/?start_date=2023-05-11&deployment=AWD_3_13082021_wildlifecamera1' | jq
```

## Running the development environment

First, checkout the [biocloud core](https://gitlab.com/arise-biodiversity/biocloud/core/biocloud-core)
project and configure and start that using the steps in the README.

Then checkout this project and start:

```
docker compose up -f docker-compose.yml -f docker-compose.override.yml -d
```

or

```
docker compose up -d
```

Do not forget to set the environment variables

```
MDS_BASE_URL=
MDS_TOKEN=
S3_ACCESS_KEY_ID=
S3_ENDPOINT=
S3_LANDING_ZONE_BUCKET=
S3_REGION_NAME=
S3_SECRET_ACCESS_KEY=
```

If you want to run a specific docker image:

```
IMAGE_VERSION=...name of the build...
```

Then start without the override:

```
docker compose -f docker-compose.yml up -d
```

## Ansible

Like biocloud core this environment installs and updates itself by using ansible in the deploy stage
of the gitlab pipeline.

Secrets and the inventory configuration are stored in the Naturalis hashicorp vault.

The ansible script sets up the data and project directory.
On the server two locations are created:

 - `/data/biocloud-mds` to store local data
 - `/opt/project/biocloud-mds` contains `.env`, `docker-compose.yml`

The actual running application is started with `docker compose up` and pulls a recently build image.

The ansible script creates the `.env` file on the running server.
Whenever new environments are added, removed or changed, please note that
these should also be added, removed or changed in these files:

 - [ansible/roles/deploy/tasks/main.yml](https://gitlab.com/arise-biodiversity/biocloud/mds/biocloud-mds/-/blob/main/ansible/roles/deploy/tasks/main.yml)
 - [docker-compose.yml](https://gitlab.com/arise-biodiversity/biocloud/mds/biocloud-mds/-/blob/main/docker-compose.yml)
 - ...and in the biocloud/ansible-biocloud-core path of the vault
 either in the host_vars or group_vars of the server.

## License
...

