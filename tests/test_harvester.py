from src.harvester import is_valid_json


class TestHarvester:
    """Unit tests for the Harvester class."""
    def test_is_valid_json(self):
        """Test if a given string is valid JSON or not"""
        json_str = '{"fruit": "apple"}'
        assert is_valid_json(json_str)
        no_json_str = 'An apple is fruit'
        assert not is_valid_json(no_json_str)
        assert not is_valid_json(None)
        assert not is_valid_json("")
