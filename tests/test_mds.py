import pytest

from src.mds.resource import Resource
from src.config import Config
from src.mds.mds import Mds


@pytest.fixture(scope="class")
def mds() -> Mds:
    """Fixture for Mds test class."""
    mds = Mds(Config.MDS_BASE_URL, Config.MDS_TOKEN)

    return mds


@pytest.mark.usefixtures("mds")
class TestMds:
    """Unit tests the Mds class."""

    def test_get_devices(self, mds: Mds) -> None:
        """Test getting devices from the MDS API."""
        devices = mds.retrieve_from_api(Resource.DEVICE)

        assert len(devices) > 0

    def test_get_deployments(self, mds: Mds) -> None:
        """Test getting deployments from the MDS API."""
        deployments = mds.retrieve_from_api(Resource.DEPLOYMENT)

        assert len(deployments) > 0

    def test_get_projects(self, mds: Mds) -> None:
        """Test getting projects from the MDS API."""
        projects = mds.retrieve_from_api(Resource.PROJECT)

        assert len(projects) > 0

    def get_file_from_api(self, mds: Mds) -> None:
        """Test getting file from the wrong API."""
        with pytest.raises(ValueError):
            mds.retrieve_from_api(Resource.FILE)

    def test_get_files(self, mds: Mds) -> None:
        """Test getting files from the MDS API."""
        deployment_id = mds.list_ids(Resource.DEPLOYMENT)[0]

        file_filter = f"deployment__in={deployment_id}&localstorage=True"

        file_metadata = mds.get_file_metadata(file_filter)

        assert file_metadata is not None
        assert len(file_metadata) > 0

        image = mds.get_image_payload(file_metadata[0]["url"])

        assert image is not None
