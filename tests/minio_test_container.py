from testcontainers.minio import MinioContainer


class MinioTestContainer(MinioContainer):
    """
        This class fixes a bug that the PostgresContainer does not start if docker runs on a dedicated host
        (DOCKER_HOST is set). See: https://github.com/testcontainers/testcontainers-python/issues/217
    """
    def get_connection_url(self, host=None) -> str:
        if host is None:
            host = self.get_docker_client().host()
            assert host is not None

        return super().get_connection_url(host)
