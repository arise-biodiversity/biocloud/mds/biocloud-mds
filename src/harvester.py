import concurrent.futures
import json
import logging

from biocloud_library.storage import S3Service
from botocore.exceptions import ParamValidationError
from datetime import datetime, date
from requests import HTTPError

from src.config import Config
from src.mds.mds import Mds
from src.mds.resource import Resource

s3 = S3Service(
    Config.S3_ACCESS_KEY_ID,
    Config.S3_SECRET_ACCESS_KEY,
    Config.S3_ENDPOINT,
    Config.S3_REGION_NAME)


def json_to_jsonl(json_list: list):
    jsonl = ''
    if json_list is None:
        return jsonl
    for item in json_list:
        jsonl += json.dumps(item) + '\n'
    return jsonl


def is_valid_json(json_data: str) -> bool:
    """Test if the provided string is valid JSON. NOTE: None or empty strings are considered not valid!"""
    if json_data is None or not json_data:
        return False
    try:
        json.loads(json_data)
    except ValueError as err:  # noqa: F841
        return False
    return True


class Harvester:
    """Class for retrieving and storing sensor data (payload and metadate) from the MDS project
    and put it as so called 'raw data' in a S3 bucket."""

    def __init__(self):
        self.mds = Mds(Config.MDS_BASE_URL, Config.MDS_TOKEN)

    def download_and_store_metadata(self) -> bool:
        """
        Download and store contextual metadata from the MDS project.
        This includes:
            - projects
            - deployments
            - devices
        The metadata is stored as JSON in an S3 bucket for raw data.
        """
        metadata = self.get_metadata()
        logging.info("Writing metadata to S3 bucket ...")
        for key in metadata.keys():
            file_name = f"mds/metadata/{key}.json"
            logging.info(f"Writing {file_name} to S3...")
            s3.put_object(Config.S3_LANDING_ZONE_BUCKET, file_name, json.dumps(metadata[key]))
        return True

    def get_metadata(self) -> dict:
        """
        Retrieve metadata about projects, deployments and devices
        and return as a dictionary
        """
        metadata = {}
        metadata["projects"] = self.mds.retrieve_from_api(Resource.PROJECT)
        logging.info("Fetched project data from MDS API.")
        metadata["deployments"] = self.mds.retrieve_from_api(Resource.DEPLOYMENT)
        logging.info("Fetched deployment data from MDS API.")
        metadata["devices"] = self.mds.retrieve_from_api(Resource.DEVICE)
        logging.info("Fetched device data from MDS API.")
        return metadata

    def download_sensor_data(self, harvest_date: date | None):
        """
        Download sensor data (payload plus metadata) from all deployments. Include
        a valid date, if you want to limit the download to a specific date.
        """
        deployment_ids = self.mds.list_ids(Resource.DEPLOYMENT)
        for deployment_id in deployment_ids:
            self.download_sensor_data_of_deployment(deployment_id, harvest_date)

    def download_sensor_data_of_deployment(self, deployment_id: str, harvest_date: date | None):
        """Store all payload data and metadata of a give deployment (and of a given date) in a S3 bucket."""
        logging.info(f"Downloading image files and associated metadata of deployment with id: {deployment_id}")

        if harvest_date is None:
            file_filter = \
                f"deployment__in={deployment_id}&localstorage=True"
        else:
            file_filter = \
                f"start_date={harvest_date}&end_date={harvest_date}&deployment__in={deployment_id}&localstorage=True"

        # Save metadata
        file_metadata = self.mds.get_file_metadata(file_filter, Config.MAX_DOWNLOAD_ATTEMPTS)
        success = self.save_metadata(deployment_id, file_metadata)
        if not success:
            logging.error(f"Downloading metadata for deployment {deployment_id} failed!")
            return
        logging.debug(f"Finished downloading metadata for deployment {deployment_id}")

        # Store payload
        logging.debug(f"Start saving payloads for deployment: {deployment_id}")
        with concurrent.futures.ThreadPoolExecutor(max_workers=Config.PARALLEL_DOWNLOADS) as executor:
            for single_file in file_metadata:
                executor.submit(self.save_payload, single_file, deployment_id)
        logging.debug(f"Finished saving payloads for deployment: {deployment_id}")

    def save_metadata(self, deployment_id: str, metadata: list) -> bool:
        """Save the file metadata of the given deployment as jsonl (JSON Lines) file in a S3 bucket."""
        logging.debug(f"Saving metadata for deployment: {deployment_id}")
        s3_dir = f'mds/deployments/{deployment_id}/processed/metadata'
        file_path = f'{s3_dir}/metadata-{int(datetime.now().timestamp())}.jsonl'
        try:
            s3.put_object(Config.S3_LANDING_ZONE_BUCKET, file_path, json_to_jsonl(metadata))
            return True
        except (ParamValidationError, HTTPError) as e:
            logging.error(f"Saving payload {file_path} failed: {e}")
            return False

    def save_payload(self, file_metadata: dict, deployment_id: str):
        """Save the payload, referred to in the file metadata, in a S3 bucket."""

        # Skip empty metadata
        if not file_metadata or file_metadata is None:
            logging.warning("Skipping empty metadata")
            return

        # Skip daily reports
        if file_metadata['format'] == '.csv':
            logging.debug(f"Skipping csv file: {file_metadata['filename']}{file_metadata['format']}")
            return

        payload = self.mds.get_image_payload(file_metadata["url"])
        if payload is None:
            logging.warning(f"Skipping empty payload file: {file_metadata['filename']}{file_metadata['format']}")
            return
        s3_dir = f"mds/deployments/{deployment_id}/payload"
        file_path = f"{s3_dir}/" + file_metadata["filename"] + file_metadata["format"]
        try:
            s3.put_object(Config.S3_LANDING_ZONE_BUCKET, file_path, payload)
        except (ParamValidationError, HTTPError) as e:
            logging.error(f"Saving payload {file_path} failed: {e}")
