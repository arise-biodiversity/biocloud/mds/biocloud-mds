import json
import logging
import os
import seqlog
import sys
import time

from src.config import Config
from datetime import datetime, date
from flask import Flask, request

from src.harvester import Harvester
from src.debugger import initialize_flask_server_debugger_if_needed

initialize_flask_server_debugger_if_needed()

logging_level = logging.getLevelName(Config.LOGGING_LEVEL)
logging.basicConfig(stream=sys.stdout, level=logging_level)

if os.environ.get('TEST') is None:
    seqlog.log_to_seq(
        server_url='http://seq:5341',
        level=logging_level,
        batch_size=10,
        auto_flush_timeout=10,
        override_root_logger=True)

app = Flask(__name__)
harvester = Harvester()


@app.route("/ping")
def ping():
    logging.info('Ping MDS')
    return json.dumps({'service': 'biocloud-mds', 'status': 'success'})


@app.route("/")
def home() -> str:
    return """
    <h2>Biocloud - Arise MDS</h2>
    <ul>
        <li><a href="mds-download-metadata">mds-download-metadata</a></li>
        <li><a href="mds-download-sensor-data-by-date">mds-download-sensor-data-by-date[?date=YYYY-MM-DD]</a></li>
        <li><a href="mds-download-all-sensor-data">mds-download-all-sensor-data</a></li>
    </ul>
    """


@app.route("/mds-download-metadata")
def collect_mds_data() -> str:
    """
    Collect MDS metadata and write it to an S3 bucket
    """
    harvester.download_and_store_metadata()
    return "Successfully downloaded and stored MDS metadata in a S3 bucket"


@app.route("/mds-download-all-sensor-data")
def get_mds_files() -> str:
    """
    Download image metadata and payloads per deployment
    and save in a S3 bucket
    """
    logging.info("Trying to download all available MDS data ...")
    harvester.download_sensor_data(None)
    return "Finished downloading sensor data (payload and associated metadata) from all deployments"


@app.route("/mds-download-sensor-data-by-date")
def download_by_date() -> str | tuple:
    """
    Download image metadata and payloads per deployment created on a specified date and
    save it in an S3 bucket. The date has to be specified as a parameter name "date" and
    must be in the format YYYY-MM-DD. If no date is provided, the date of today is being used.
    """
    harvest_date = request.args.get('date')
    try:
        harvest_date = datetime.strptime(harvest_date.strip(), '%Y-%m-%d').date()
    except (ValueError, AttributeError):
        return f"Invalid date: {harvest_date}. Please provide a valid date using the format: \"YYYY-MM-DD\". " \
               f"E.g.: ?date={str(date.today())}", 400
    logging.info(f"Date used for downloading image data: {harvest_date}")
    start_time = time.time()
    harvester.download_sensor_data(harvest_date)
    duration = time.time() - start_time
    logging.info(f"Finished download in {int(duration)} seconds")
    return f"Finished downloading sensor data (payload and associated metadata) of all deployments " \
           f"created on: {harvest_date}."
