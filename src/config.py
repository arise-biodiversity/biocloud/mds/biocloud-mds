import os
from typing import get_type_hints, Union
from dotenv import load_dotenv

load_dotenv()


class AppConfigError(Exception):
    pass


def _parse_bool(val: Union[str, bool]) -> bool:  # pylint: disable=E1136
    return val if isinstance(val, bool) else val.lower() in ['true', 'yes', '1']


# AppConfig class with required fields, default values, type checking, and typecasting for int and bool values
class AppConfig:
    DEBUGGER: bool = False
    LOGGING_LEVEL: str = 'INFO'

    MDS_BASE_URL: str
    MDS_TOKEN: str
    S3_REGION_NAME: str = 'eu-west-1'
    S3_ACCESS_KEY_ID: str
    S3_SECRET_ACCESS_KEY: str
    S3_LANDING_ZONE_BUCKET: str
    S3_ENDPOINT: str = 'http://s3.eu-west-1.amazonaws.com'
    MAX_DOWNLOAD_ATTEMPTS: int = 5
    PARALLEL_DOWNLOADS: int = 10
    PAYLOAD_REQUEST_TIMEOUT_CONNECT: int = 30
    PAYLOAD_REQUEST_TIMEOUT_RESPONSE: int = 150

    """
    Map environment variables to class fields according to these rules:
      - Field won't be parsed unless it has a type annotation
      - Field will be skipped if not in all caps
      - Class field and environment variable name are the same
    """

    def __init__(self, env):
        for field in self.__annotations__:
            if not field.isupper():
                continue

            # Raise AppConfigError if required field not supplied
            default_value = getattr(self, field, None)
            if default_value is None and env.get(field) is None:
                raise AppConfigError('The {} field is required'.format(field))

            # Cast env var value to expected type and raise AppConfigError on failure
            try:
                var_type = get_type_hints(AppConfig)[field]
                if var_type == bool:
                    value = _parse_bool(env.get(field, default_value))
                else:
                    value = var_type(env.get(field, default_value))

                self.__setattr__(field, value)
            except ValueError:
                raise AppConfigError('Unable to cast value of "{}" to type "{}" for "{}" field'.format(
                    env[field],
                    var_type,
                    field
                ))

    def __repr__(self):
        return str(self.__dict__)


# Expose Config object for app to import
Config = AppConfig(os.environ)
