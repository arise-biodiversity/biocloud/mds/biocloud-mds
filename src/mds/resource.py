from enum import Enum


class Resource(Enum):
    DEVICE = "Device"
    DEPLOYMENT = "Deployment"
    FILE = "File"
    PROJECT = "Project"

    @classmethod
    def is_valid(cls, resource) -> bool:
        resources = [r.value for r in Resource]
        return resource in resources
