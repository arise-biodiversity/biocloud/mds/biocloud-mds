import logging

import requests
import time
from typing import Optional

from src.config import Config
from src.mds.resource import Resource


REQUEST_TIMEOUT = (Config.PAYLOAD_REQUEST_TIMEOUT_CONNECT,
                   Config.PAYLOAD_REQUEST_TIMEOUT_RESPONSE)


class Mds:
    """Class for querying the MDS API."""

    def __init__(self, base_url: str, token: str):
        self.base_url = base_url.rstrip("/")
        self.api_url = f"{base_url}/api"
        self.token = token

    def retrieve_from_api(self, resource: Resource, retries: int = 5) -> list | None:
        """Fetch JSON data from the MDS API."""

        if not isinstance(resource, Resource):
            raise ValueError(f"Not a valid MDS resource: {resource}.")

        if resource == Resource.FILE:
            raise ValueError(
                "Cannot retrieve files from the MDS API using this method. Use get_file_metadata() instead."
            )

        for attempt in range(retries):
            try:
                response = requests.get(
                    f"{self.api_url}/{resource.value}",
                    headers={
                        "Authorization": f"Token {self.token}"
                    })

                response.raise_for_status()

                # If we made it this far, the request was successful, return the result
                return response.json()

            except (requests.exceptions.RequestException, requests.exceptions.HTTPError) as e:
                if isinstance(e, requests.exceptions.HTTPError) and response.status_code < 500:
                    # For HTTP errors that aren't server errors, don't retry
                    logging.error(f"{response.status_code} {response.reason}")
                    raise e

                if attempt < retries - 1:  # No need to sleep on last attempt
                    sleep_time = 2 ** attempt  # exponential back-off
                    logging.warning(f"Request failed, waiting {sleep_time} seconds before retrying...")
                    time.sleep(sleep_time)
                else:
                    logging.error(f"Request failed after {retries} attempts.")
                    raise e

        # This point will never be reached because the loop either returns a result
        # or raises an exception, but it's here to satisfy any possible linter errors
        return None

    def get_file_metadata(self, file_filter: str, retries: int = 5) -> list | None:
        """
        Fetch File metadata from the MDS API
        """

        for attempt in range(retries):
            try:
                response = requests.get(
                    f"{self.api_url}/{Resource.FILE.value}/?{file_filter}",
                    headers={
                        "Authorization": f"Token {self.token}"
                    })

                response.raise_for_status()

                # If we made it this far, the request was successful, return the result
                return response.json()

            except (requests.exceptions.RequestException, requests.exceptions.HTTPError) as e:
                if isinstance(e, requests.exceptions.HTTPError) and response.status_code < 500:
                    # For HTTP errors that aren't server errors, don't retry
                    logging.error(f"{response.status_code} {response.reason}")
                    raise e

                if attempt < retries - 1:  # No need to sleep on last attempt
                    sleep_time = 2 ** attempt  # exponential back-off
                    logging.warning(f"Request failed, waiting {sleep_time} seconds before retrying...")
                    time.sleep(sleep_time)
                else:
                    logging.error(f"Request failed after {retries} attempts.")
                    raise e

        # This point will never be reached because the loop either returns a result
        # or raises an exception, but it's here to satisfy any possible linter errors
        return None

    def get_image_payload(self, file_path: str, retries: int = 5) -> Optional[bytes]:
        """Get image payload from server"""

        for attempt in range(retries):
            try:
                media_request = requests.get(
                    f"{self.base_url}/{file_path}",
                    headers={"Authorization": f"Token {self.token}"},
                    timeout=REQUEST_TIMEOUT)

                if media_request.headers["Content-length"] == "0":
                    logging.warning(
                        f"SKIPPING: File {file_path} not available - status code = {media_request.status_code}"
                    )
                    return None

                if not media_request.ok:
                    logging.warning(f"status code is not OK => {media_request.status_code}")
                    return None

                media_request.raise_for_status()

                # If we made it this far, the request was successful, return the result
                return media_request.content

            except (requests.exceptions.RequestException, requests.exceptions.HTTPError) as e:
                if isinstance(e, requests.exceptions.HTTPError) and media_request.status_code < 500:
                    # For HTTP errors that aren't server errors, don't retry
                    logging.error(f"{media_request.status_code} {media_request.reason}")
                    raise e

                if attempt < retries - 1:  # No need to sleep on last attempt
                    sleep_time = 2 ** attempt  # exponential back-off
                    logging.warning(f"Request failed, waiting {sleep_time} seconds before retrying...")
                    time.sleep(sleep_time)
                else:
                    logging.error(f"Request failed after {retries} attempts.")
                    raise e

        # This point will never be reached because the loop either returns a result
        # or raises an exception, but it's here to satisfy any possible linter errors
        return None

    def list_ids(self, resource: Resource) -> list:
        """List IDs of the specified Resource."""
        objs = self.retrieve_from_api(resource)
        obj_ids = []
        for obj in objs:
            obj_ids.append(obj["id"])
        return obj_ids
