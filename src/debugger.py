import multiprocessing
import debugpy

from src.config import Config


def initialize_flask_server_debugger_if_needed():
    if Config.DEBUGGER is True:

        # In debug mode, Flask uses a first process (with pid==1) to start child processes that handle connections.
        # If the code below this line is executed by the main process, the debugging port is taken and subsequent
        # child processes can't use the same port and are attributed a random port which prevents connections.

        if multiprocessing.current_process().pid > 1:
            debugpy.listen(("0.0.0.0", 5680))
            print("⏳ VS Code debugger can now be attached, press F5 in VS Code ⏳", flush=True)
            debugpy.wait_for_client()
            print("🎉 VS Code debugger attached, enjoy debugging 🎉", flush=True)
